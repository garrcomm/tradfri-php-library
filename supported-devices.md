# Supported devices

This library will work with probably all devices,
but since this is a hobby project and I have a limited budget and some products are discontinued,
I was only be able to test with a short list of devices.

I tried to get a wide spectrum of device types though, so all features could be tested.

Below you'll find a list of all devices that this library is tested with.
If you have tested other devices, please let me know your results!

Article number | Product name                                                      
-------------- | ------------------------------------------------------------------
[403.378.06]   | TRÅDFRI   Gateway                                                  
[104.004.08]   | TRÅDFRI   Signal repeater                                          
[503.561.87]   | TRÅDFRI   Driver for wireless control, 10W                         
[603.426.56]   | TRÅDFRI   Driver for wireless control, 30W                         
[904.087.97]   | TRÅDFRI   LED bulb E27 WW (Warm white) 806 lumen                   
[604.084.83]   | TRÅDFRI   LED bulb E27 WS (White spectrum) opal 1000 lumen         
[504.115.65]   | TRÅDFRI   LED bulb E14 CWS (Color / White spectrum) opal 600 lumen 
[903.561.66]   | TRÅDFRI   Wireless control outlet                                  
[304.431.24]   | TRÅDFRI   Remote control                                           
[403.563.81]   | TRÅDFRI   Shortcut button                                          
[704.085.95]   | TRÅDFRI   Wireless dimmer                                          
[704.299.13]   | TRÅDFRI   Wireless motion sensor                                   
[603.704.80]   | SYMFONISK Sound Remote                                             

[403.378.06]: https://www.ikea.com/nl/en/p/tradfri-gateway-white-40337806/
[104.004.08]: https://www.ikea.com/nl/en/p/tradfri-signal-repeater-10400408/
[503.561.87]: https://www.ikea.com/nl/en/p/tradfri-driver-for-wireless-control-grey-50356187/
[603.426.56]: https://www.ikea.com/nl/en/p/tradfri-driver-for-wireless-control-grey-60342656/
[904.087.97]: https://www.ikea.com/nl/en/p/tradfri-led-bulb-e27-806-lumen-wireless-dimmable-warm-white-globe-opal-white-90408797/
[604.084.83]: https://www.ikea.com/nl/en/p/tradfri-led-bulb-e27-1000-lumen-wireless-dimmable-white-spectrum-globe-opal-white-60408483/
[004.086.12]: https://www.ikea.com/nl/en/p/tradfri-led-bulb-e27-600-lumen-wireless-dimmable-colour-and-white-spectrum-globe-opal-white-00408612/
[504.115.65]: https://www.ikea.com/nl/en/p/tradfri-led-bulb-e14-600-lumen-wireless-dimmable-colour-and-white-spectrum-globe-opal-white-50411565/
[903.561.66]: https://www.ikea.com/nl/en/p/tradfri-wireless-control-outlet-90356166/
[304.431.24]: https://www.ikea.com/nl/en/p/tradfri-remote-control-30443124/
[403.563.81]: https://www.ikea.com/nl/en/p/tradfri-shortcut-button-white-40356381/
[704.085.95]: https://www.ikea.com/nl/en/p/tradfri-wireless-dimmer-white-70408595/
[704.299.13]: https://www.ikea.com/nl/en/p/tradfri-wireless-motion-sensor-white-70429913/
[603.704.80]: https://www.ikea.com/nl/en/p/symfonisk-sound-remote-white-60370480/
