# Ikea TRÅDFRI Library for PHP

**This code is currently very much in progress. It's interfaces will probably change a lot before the first release.**

This library connects to an IKEA TRÅDFRI Gateway using CoAP and provides a simple web interface to interact with your IKEA Smart Home devices.

This repository only contains the library. To get a fully working client, see [https://bitbucket.org/garrcomm/tradfri-web-application](https://bitbucket.org/garrcomm/tradfri-web-application).

## How to use

First, initiate a TRÅDFRI client.
It's best to store the client data and re-use that, since the gateway doesn't like many clients in memory.

```php
<?php
$ip = '192.168.2.170';
$securityCode = 'f00b4r';

// Initiate a TRÅDFRI client
$tradfri = new \Garrcomm\Tradfri\Service\Tradfri($ip, '/usr/local/bin/coap-client');

// First time; fetch a client ID and pre-shared key
$clientData = $tradfri->authenticate($securityCode);
```

When we have the client data, we can continue like this:

```php
$ip = '192.168.2.170';

// Initiate a TRÅDFRI client
$tradfri = new \Garrcomm\Tradfri\Service\Tradfri($ip, '/usr/local/bin/coap-client');

// Log in using known credentials
$tradfri->setClientIdentity($clientData['clientIdentity'], $clientData['privateSharedKey']);

// Turn on all plugs that are currently off
foreach($tradfri->listDevices() as $device) {
    if ($device instanceof \Garrcomm\Tradfri\Model\TradfriPlug && !$device->isOn()) {
        $device->turnOn();
    }
}
```

A device can be any class extending [BaseTradfriDevice](src/Model/BaseTradfriDevice.php).
Each device can have its own capabilities. There are several ways to see which capabilities a device has:

```php
// instanceof check
if ($device instanceof \Garrcomm\Tradfri\Model\TradfriLight) {
    // I'm a lamp, so you can display me with a lamp icon.
}

// method_exists check
if (method_exists($device, 'turnOn')) {
    // I'm either a plug, or a lamp, or maybe something else? But you can turn me on!
}
if (method_exists($device, 'setBrightness')) {
    // I'm very likely some kind of a lamp. You can brighten me up!
}
```

To see more usages, check out the code in https://bitbucket.org/garrcomm/tradfri-web-application

## coap-client dependency

For PHP, we don't have many dependencies. Just `PHP 7.3+` and `ext-json` (which is installed in PHP by default).
But we do need a coap-client binary. There are several approaches.

In my [Web application example](https://bitbucket.org/garrcomm/tradfri-web-application), a docker container is included that already has this binary.
You can also compile it yourself on your machine by executing the script found at [https://github.com/glenndehaan/ikea-tradfri-coap-docs/tree/master/scripts](https://github.com/glenndehaan/ikea-tradfri-coap-docs/tree/master/scripts).

A 3rd option, during development, you can use the `coap-client` simulator that's available in this repository in the [tests/CoapClient](tests/CoapClient) folder.
This script is also used for unit tests and simulates my own IKEA Home setup.

## Tested with IKEA TRÅDFRI products

For testing, I started with the devices I already had in my home. Later I purchased a few extras to be able to test more functionalities.
See [supported-devices.md](supported-devices.md) for a full list of devices I used during tests.

Other devices will probably also work, but I haven't tested them.
Also, keep in mind that sensors/remote controls/dimmers are input devices. Just as this app, they're ment to write to output devices. So although I detect those devices, it's not possible to interact with them, except for reading their battery state and meta data.

On my wishlist I have these items:

* [FYRTUR    Block-out roller blind](https://www.ikea.com/nl/en/p/fyrtur-block-out-roller-blind-wireless-battery-operated-grey-40408196/)  
  My home office has two windows next to each other, one of 160cm and one of 60cm wide. The current sizes of the FYRTUR series won't cover this whole set of windows.
  But it would be nice if this project also supported the blinds in the future.
* [TRÅDFRI   LED bulb GU10 CWS opal 345 lumen](https://www.ikea.com/nl/en/p/tradfri-led-bulb-gu10-345-lumen-wireless-dimmable-colour-and-white-spectrum-80439228/)  
  On my attic I have four spots on my Lego train table. At some point I want to add multicolor lights there.

## Tip for Windows Developers

In the `bin` folder, a few batch files exist, to make development easier.

If you install [Docker Desktop for Windows](https://www.docker.com/products/docker-desktop),
you can use [bin\composer.cmd](bin/composer.cmd), [bin\phpcs.cmd](bin/phpcs.cmd), [bin\phpunit.cmd](bin/phpunit.cmd) and [bin\security-checker.cmd](bin\security-checker.cmd) as shortcuts for Composer, CodeSniffer, PHPUnit and the Security Checker, without the need of installing PHP and other dependencies on your machine.

The same Docker container and tools are used in [Bitbucket Pipelines](https://bitbucket.org/garrcomm/tradfri-php-library/addon/pipelines/home) to automatically test this project.

## External resources

Before I started on this project, I used Google to find something simular. I did not find much PHP related that I could use, but I found some useful resources:

* A basic PHP-wrapper for the libcoap coap-client. I didn't use this because it only has 1 commit and is not a complete application, nor a ready-to-use package, but it was a good inspiration nonetheless;  
  https://github.com/MiniMeOSc/phpTradfri
* Very well written documentation on how the TRÅDFRI gateway works with CoAP:  
  https://github.com/glenndehaan/ikea-tradfri-coap-docs
* A list of many constants that helped identifying the numeric values in the CoAP protocol:  
  https://gist.github.com/r41d/5d62033f88b3046bccf406c9158d4e59
