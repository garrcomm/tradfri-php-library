<?php

namespace Garrcomm\Tradfri\Exception;

class TradfriException extends \Exception
{
    /**
     * A specified parameters value is too high or too low
     */
    public const VALUE_OUT_OF_RANGE = 1;
    /**
     * The IPv4 address specified for the gateway is not correctly formatted
     */
    public const INVALID_IPV4_ADDRESS = 2;
    /**
     * The coap-client path could not be found or is not executable
     */
    public const INVALID_COAP_CLIENT = 3;
    /**
     * The item you are looking for can't be found
     */
    public const ITEM_NOT_FOUND = 4;
    /**
     * The authentication has failed
     */
    public const AUTHENTICATION_FAILED = 5;
    /**
     * The connection has failed (this is difficult to exactly detect; sometimes we just don't get a normal response)
     */
    public const CONNECTION_FAILED = 6;
    /**
     * When the coap-client errors with a response code, COAP_CLIENT_ERROR will be added, so $? = 1 will result in 1001.
     */
    public const COAP_CLIENT_ERROR = 1000;
    /**
     * When we get an error code from the coap server, COAP_SERVER_ERROR will be added, so 4.04 will result in 2404.
     */
    public const COAP_SERVER_ERROR = 2000;

    /**
     * Additional debug info, when available.
     *
     * @var array|null
     */
    private $debugInfo;

    /**
     * Creates a new TradfriException
     *
     * @param string     $message   Human readable error message.
     * @param integer    $code      One of the TradfriException:: constants.
     * @param array|null $debugInfo Additional debug info, when available.
     */
    public function __construct(string $message = "", int $code = 0, array $debugInfo = null)
    {
        $this->debugInfo = $debugInfo;
        parent::__construct($message, $code);
    }

    /**
     * Returns additional debug info, when available.
     *
     * @return array|null
     */
    public function getDebugInfo(): ?array
    {
        return $this->debugInfo;
    }
}
