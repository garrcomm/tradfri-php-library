<?php

namespace Garrcomm\Tradfri\Model;

class TradfriBlind extends BaseTradfriDevice
{
    private const
        BLIND = 15015,
        POSITION = 5536;

    /**
     * Gets the position of the blind
     *
     * @return float
     */
    public function getPosition(): float
    {
        return (float)$this->deviceData[static::BLIND][0][static::POSITION];
    }

    /**
     * Sets the position of the blind
     *
     * @param float $position A value from 0 to 100.
     *
     * @return void
     */
    public function setPosition(float $position): void
    {
        $this->tradfri->writeToDevice($this->getId(), [static::BLIND => [[
            static::POSITION => $position,
        ]]]);
        $this->deviceData[static::BLIND][0][static::POSITION] = $position;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), array(
            'position' => $this->getPosition(),
        ));
    }
}
