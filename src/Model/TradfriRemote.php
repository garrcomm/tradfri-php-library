<?php

namespace Garrcomm\Tradfri\Model;

class TradfriRemote extends BaseTradfriDevice
{
//    private const
//        SENSOR = 3300,
//        SWITCH = 15009
//    ;

    /**
     * Returns the battery state in percentage
     *
     * @return integer
     */
    public function getBatteryLevel(): int
    {
        return $this->deviceData[static::DEVICE][9];
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), array(
            'batteryLevel' => $this->getBatteryLevel(),
        ));
    }
}
