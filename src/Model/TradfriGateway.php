<?php

namespace Garrcomm\Tradfri\Model;

use Garrcomm\Tradfri\Service\Tradfri;

class TradfriGateway implements \JsonSerializable
{
    private const
        VERSION = 9029
    ;

    /**
     * Reference to the Hub this group is connected by
     *
     * @var Tradfri
     */
    private $tradfri;

    /**
     * Raw data as received from the TRÅDFRI Gateway
     *
     * @var array
     */
    private $gatewayData;

    /**
     * Initializes the TRÅDFRI Gateway
     *
     * @param Tradfri $tradfri     The TRÅDFRI Gateway on which the group exists.
     * @param array   $gatewayData Raw data as received from the TRÅDFRI Gateway.
     */
    public function __construct(Tradfri $tradfri, array $gatewayData)
    {
        $this->tradfri = $tradfri;
        $this->gatewayData = $gatewayData;
    }

    /**
     * Returns the version of the devices firmware
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->gatewayData[static::VERSION];
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array(
            'version' => $this->getVersion(),
            //'raw'     => $this->gatewayData,
        );
    }
}
