<?php

namespace Garrcomm\Tradfri\Model;

use Garrcomm\Tradfri\Exception\TradfriException;

class TradfriColorLight extends TradfriLight
{
    private const
        COLOR = 5706,
        UNKNOWN_PROPERTY_KEY1 = 5707, // hue?
        UNKNOWN_PROPERTY_KEY2 = 5708, // saturation?
        COLOR_X = 5709,
        COLOR_Y = 5710
    ;

    /**
     * A list of available RGB values
     */
    private const
        AVAILABLE_RGB_COLORS_CWS = array(
            "4a418a" => "Blue",
            "6c83ba" => "Light Blue",
            "8f2686" => "Saturated Purple",
            "a9d62b" => "Lime",
            "c984bb" => "Light Purple",
            "d6e44b" => "Yellow",
            "d9337c" => "Saturated Pink",
            "da5d41" => "Dark Peach",
            "dc4b31" => "Saturated Red",
            "dcf0f8" => "Cold sky",
            "e491af" => "Pink",
            "e57345" => "Peach",
            "e78834" => "Warm Amber",
            "e8bedd" => "Light Pink",
            "eaf6fb" => "Cool daylight",
            "ebb63e" => "Candlelight",
            "efd275" => "Warm glow",
            "f1e0b5" => "Warm white",
            "f2eccf" => "Sunrise",
            "f5faf6" => "Cool white",
        ),
        AVAILABLE_RGB_COLORS_WS = array(
            "f5faf6" => "White",
            "f1e0b5" => "Warm",
            "efd275" => "Glow",
        );

    /**
     * Returns the color in RGB values
     *
     * @return string
     */
    public function getRgbColor(): string
    {
        return $this->deviceData[static::LIGHT][0][static::COLOR];
    }

    /**
     * Returns a list of available colors; the array keys are color codes, the values are color descriptions.
     *
     * @return string[]
     */
    public function getAvailableRgbColors(): array
    {
        if (preg_match('/\sCWS\s/', $this->getProductName())) { // Color + White Spectrum
            return static::AVAILABLE_RGB_COLORS_CWS;
        } elseif (preg_match('/\sWS\s/', $this->getProductName())) { // White Spectrum
            return static::AVAILABLE_RGB_COLORS_WS;
        }
        throw new \RuntimeException('Could\'nt determine the lamp type');
    }

    /**
     * Returns the X-value for the CIE 1931 color code
     *
     * @return string
     */
    public function getColorX(): string
    {
        return $this->deviceData[static::LIGHT][0][static::COLOR_X];
    }

    /**
     * Returns the Y-value for the CIE 1931 color code
     *
     * @return string
     */
    public function getColorY(): string
    {
        return $this->deviceData[static::LIGHT][0][static::COLOR_Y];
    }

    /**
     * Sets the color in RGB values
     *
     * @param string $color Color in RGB values as 6 hexadecimal digits.
     *
     * @return void
     */
    public function setRgbColor(string $color): void
    {
        $color = strtolower($color);
        if (!preg_match('/^[0-9a-f]{6}$/', $color)) {
            throw new TradfriException(
                'RGB color should be 6 hexadecimal digits',
                TradfriException::VALUE_OUT_OF_RANGE
            );
        }

        // Ikea has a very specific subset of hex values that are accepted
        // @TODO Calculate RGB to Xy or HS values to support the full spectrum
        if (!array_key_exists($color, $this->getAvailableRgbColors())) {
            throw new TradfriException(
                'Invalid color code. See the getAvailableRgbColors() method for available colors',
                TradfriException::VALUE_OUT_OF_RANGE
            );
        }

        $this->tradfri->writeToDevice($this->getId(), [static::LIGHT => [[
            static::COLOR => $color,
        ]]]);
        $this->deviceData[static::LIGHT][0][static::COLOR] = $color;
    }

    /**
     * Converts a color code like 'e491af' into an array of 3 RGB integers: 228, 145, 175
     *
     * @param string $color The color code as string.
     *
     * @return integer[] The RGB values
     *
     * @todo Is this method going to be used? If not, remove it.
     */
    private function rgbStringToArray(string $color): array
    {
        $return = array();
        for ($i = 0; $i < strlen($color); $i = $i + 2) {
            $return[] = hexdec(substr($color, $i, 2));
        }
        return $return;
    }

    /**
     * Converts an RGB value to an XY (CIE 1931) value
     *
     * The formula has been found at https://stackoverflow.com/questions/54663997/convert-rgb-color-to-xy
     *
     * @param integer $r The red value from 0 to 255.
     * @param integer $g The green value from 0 to 255.
     * @param integer $b The blue value from 0 to 255.
     *
     * @return int[] It's safe to execute list($x, $y) = $this->RgbToXy($r, $g, $b);
     *
     * @todo Is this method going to be used? If not, remove it.
     */
    private function rgbToXy(int $r, int $g, int $b): array
    {
        if ($r < 0 || $r > 255 || $g < 0 || $g > 255 || $b < 0 || $b > 255) {
            throw new TradfriException(
                'RGB values should be 0 to 255 each',
                TradfriException::VALUE_OUT_OF_RANGE
            );
        }

        $redC = ($r / 0xff);
        $greenC = ($g / 0xff);
        $blueC = ($b / 0xff);

        $redN = ($redC > 0.04045) ? pow(($redC + 0.055) / (1.0 + 0.055), 2.4) : ($redC / 12.92);
        $greenN = ($greenC > 0.04045) ? pow(($greenC + 0.055) / (1.0 + 0.055), 2.4) : ($greenC / 12.92);
        $blueN = ($blueC > 0.04045) ? pow(($blueC + 0.055) / (1.0 + 0.055), 2.4) : ($blueC / 12.92);

        $X = $redN * 0.664511 + $greenN * 0.154324 + $blueN * 0.162028;
        $Y = $redN * 0.283881 + $greenN * 0.668433 + $blueN * 0.047685;
        $Z = $redN * 0.000088 + $greenN * 0.072310 + $blueN * 0.986039;

        $_x = $X / ($X + $Y + $Z);
        $_y = $Y / ($X + $Y + $Z);

        return [intval($_x * 0xffff, 10), intval($_y * 0xffff, 10)];
    }

    /**
     * Sets the color in XY (CI 1931) values
     *
     * @param integer $x A value from 0 to 65535.
     * @param integer $y A value from 0 to 65535.
     *
     * @return void
     */
    public function setXyColor(int $x, int $y): void
    {
        if ($x < 0 || $x > 65535 || $y < 0 || $y > 65535) {
            throw new TradfriException(
                'X and Y should be a value from 0 to 65535',
                TradfriException::VALUE_OUT_OF_RANGE
            );
        }

        $this->tradfri->writeToDevice($this->getId(), [static::LIGHT => [[
            static::COLOR_X => $x,
            static::COLOR_Y => $y,
        ]]]);
        $this->deviceData[static::LIGHT][0][static::COLOR_X] = $x;
        $this->deviceData[static::LIGHT][0][static::COLOR_Y] = $y;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), array(
            'on'       => $this->isOn(),
            'rgbColor' => $this->getRgbColor(),
            'colorX'   => $this->getColorX(),
            'colorY'   => $this->getColorY(),
            //'rawLight' => $this->deviceData[static::LIGHT][0],
        ));
    }
}
