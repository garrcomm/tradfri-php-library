<?php

namespace Garrcomm\Tradfri\Model;

use Garrcomm\Tradfri\Service\Tradfri;

class TradfriScene implements \JsonSerializable
{
    private const
        ONOFF = 5850,
        NAME = 9001,
        CREATED_AT = 9002,
        INSTANCE_ID = 9003,
        SCENE_ID = 9039,
        IKEA_ICON_INDEX = 9109,
        SCENE_INDEX = 9057,
        IKEA_MOODS = 9068,
        LIGHT_SETTING = 15013,
        UNKNOWN_PROPERTY_KEY0 = 15020
    ;

    /**
     * Raw data as received from the TRÅDFRI Gateway
     *
     * @var array
     */
    protected $sceneData;

    /**
     * Reference to the Hub this device is connected by
     *
     * @var Tradfri
     */
    protected $tradfri;

    /**
     * Initializes a TRÅDFRI Scene
     *
     * @param Tradfri $tradfri   The TRÅDFRI Gateway on which the device exists.
     * @param array   $sceneData Raw data as received from the TRÅDFRI Gateway.
     */
    public function __construct(Tradfri $tradfri, array $sceneData)
    {
        $this->tradfri = $tradfri;
        $this->sceneData = $sceneData;
    }

    /**
     * Returns the instance ID of the scene
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->sceneData[static::INSTANCE_ID];
    }

    /**
     * Returns the name of the scene
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->sceneData[static::NAME];
    }

    /**
     * Returns the index of the icon in use
     *
     * 1 = Desk lamp
     * 2 = Moon
     * 3 = Broom
     * 4 = Party poppet
     * etc.
     *
     * @return integer
     */
    public function getIconIndex(): int
    {
        return $this->sceneData[static::IKEA_ICON_INDEX];
    }

    /**
     * Returns the timestamp on which this item is created
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return new \DateTime('@' . $this->sceneData[static::CREATED_AT]);
    }

    /**
     * Executes the scene
     *
     * @return void
     */
    public function execute(): void
    {
        $this->tradfri->writeToGroup($this->sceneData['_groupId'], [
            static::ONOFF => 1,
            static::SCENE_ID => $this->getId(),
        ]);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array(
            'id'        => $this->getId(),
            'name'      => $this->getName(),
            'iconIndex' => $this->getIconIndex(),
            'createdAt' => $this->getCreatedAt()->format('c'),
            //'raw'       => $this->sceneData,
        );
    }
}
