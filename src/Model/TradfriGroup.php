<?php

namespace Garrcomm\Tradfri\Model;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Service\Tradfri;

class TradfriGroup implements \JsonSerializable
{
    private const
        ONOFF = 5850,
        DIMMER = 5851,
        NAME = 9001,
        CREATED_AT = 9002,
        INSTANCE_ID = 9003,
        UNKNOWN_PROPERTY_KEY0 = 9108,
        HS_ACCESSORY_LINK = 9018,
        SCENE_ID = 9039,
        HS_LINK = 15002
    ;

    /**
     * Raw data as received from the TRÅDFRI Gateway
     *
     * @var array
     */
    private $groupData;

    /**
     * Reference to the Hub this group is connected by
     *
     * @var Tradfri
     */
    private $tradfri;

    /**
     * Initializes a TRÅDFRI Group
     *
     * @param Tradfri $tradfri   The TRÅDFRI Gateway on which the group exists.
     * @param array   $groupData Raw data as received from the TRÅDFRI Gateway.
     */
    public function __construct(Tradfri $tradfri, array $groupData)
    {
        $this->tradfri = $tradfri;
        $this->groupData = $groupData;
    }

    /**
     * Returns the instance ID of the group
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->groupData[static::INSTANCE_ID];
    }

    /**
     * Returns the name of the group
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->groupData[static::NAME];
    }

    /**
     * Returns the timestamp on which this item is created
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return new \DateTime('@' . $this->groupData[static::CREATED_AT]);
    }

    /**
     * Returns a list of all devices in this group
     *
     * @return BaseTradfriDevice[]
     */
    public function getDevices(): array
    {
        $return = array();
        foreach ($this->groupData[static::HS_ACCESSORY_LINK][static::HS_LINK][static::INSTANCE_ID] as $deviceId) {
            $return[$deviceId] = $this->tradfri->getDevice($deviceId);
        }
        return $return;
    }

    /**
     * Turns on all devices in the group
     *
     * @return void
     */
    public function turnOn(): void
    {
        $this->tradfri->writeToGroup($this->getId(), [
            static::ONOFF => 1,
        ]);
        $this->refreshDevices();
    }

    /**
     * Turns off all devices in the group
     *
     * @return void
     */
    public function turnOff(): void
    {
        $this->tradfri->writeToGroup($this->getId(), [
            static::ONOFF => 0,
        ]);
        $this->refreshDevices();
    }

    /**
     * Sets the brightness of all capable devices in the group.
     *
     * @param integer $brightness The new value (an integer value from 0 to 254).
     *
     * @return void
     */
    public function setBrightness(int $brightness): void
    {
        if ($brightness < 0 || $brightness > 254) {
            throw new TradfriException(
                'The brightness should be a value from 0 to 254',
                TradfriException::VALUE_OUT_OF_RANGE
            );
        }
        $this->tradfri->writeToGroup($this->getId(), [
            static::DIMMER => $brightness,
        ]);
        $this->refreshDevices();
    }

    /**
     * After a turnOn, turnOff or setBrightness we want to reload the state of all devices
     *
     * @return void
     */
    private function refreshDevices(): void
    {
        // Update status of all devices within this group
        foreach ($this->getDevices() as $tradfriDevice) {
            $this->tradfri->getDevice($tradfriDevice->getId(), true);
        }
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array(
            'id'        => $this->getId(),
            'name'      => $this->getName(),
            'devices'   => $this->getDevices(),
            'createdAt' => $this->getCreatedAt()->format('c'),
            //'raw'       => $this->groupData,
        );
    }
}
