<?php

namespace Garrcomm\Tradfri\Model;

use Garrcomm\Tradfri\Exception\TradfriException;

class TradfriLight extends BaseTradfriDevice
{
    protected const
        LIGHT = 3311,
        UNKNOWN_PROPERTY_KEY0 = 5849, // Always 2?
        ONOFF = 5850,
        DIMMER = 5851
    ;

    /**
     * Returns true if the light is on, otherwise false.
     *
     * @return boolean
     */
    public function isOn(): bool
    {
        return (bool)$this->deviceData[static::LIGHT][0][static::ONOFF];
    }

    /**
     * Returns the brightness value from 0 to 254
     *
     * @return integer
     */
    public function getBrightness(): int
    {
        return $this->deviceData[static::LIGHT][0][static::DIMMER];
    }

    /**
     * Turns on the device
     *
     * @return void
     */
    public function turnOn(): void
    {
        $this->tradfri->writeToDevice($this->getId(), [static::LIGHT => [[
            static::ONOFF => 1,
        ]]]);
        $this->deviceData[static::LIGHT][0][static::ONOFF] = 1;
    }

    /**
     * Turns off the device
     *
     * @return void
     */
    public function turnOff(): void
    {
        $this->tradfri->writeToDevice($this->getId(), [static::LIGHT => [[
            static::ONOFF => 0,
        ]]]);
        $this->deviceData[static::LIGHT][0][static::ONOFF] = 0;
    }

    /**
     * Sets the brightness of the device
     *
     * @param integer $brightness The new value (an integer value from 0 to 254).
     *
     * @return void
     */
    public function setBrightness(int $brightness): void
    {
        if ($brightness < 0 || $brightness > 254) {
            throw new TradfriException(
                'The brightness should be a value from 0 to 254',
                TradfriException::VALUE_OUT_OF_RANGE
            );
        }
        $this->tradfri->writeToDevice($this->getId(), [static::LIGHT => [[
            static::DIMMER => $brightness,
        ]]]);
        $this->deviceData[static::LIGHT][0][static::DIMMER] = $brightness;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), array(
            'on'         => $this->isOn(),
            'brightness' => $this->getBrightness(),
        ));
    }
}
