<?php

namespace Garrcomm\Tradfri\Model;

use Garrcomm\Tradfri\Service\Tradfri;

abstract class BaseTradfriDevice implements \JsonSerializable
{
    protected const
        // The DEVICE collection has the following subitems:
        // 3.0 = Manufacturer
        // 3.1 = Product name
        // 3.2 = always empty?
        // 3.3 = FW version
        // 3.6 = 1 or 3?
        // 3.7 = Product group ID?
        // 3.9 = Battery level
        DEVICE = 3,
        NAME = 9001,
        CREATED_AT = 9002,
        INSTANCE_ID = 9003,
        REACHABILITY_STATE = 9019,
        LAST_SEEN = 9020,
        OTA_UPDATE_STATE = 9054
    ;

    /**
     * Raw data as received from the TRÅDFRI Gateway
     *
     * @var array
     */
    protected $deviceData;

    /**
     * Reference to the Hub this device is connected by
     *
     * @var Tradfri
     */
    protected $tradfri;

    /**
     * Initializes a Generic TRÅDFRI Device
     *
     * @param Tradfri $tradfri    The TRÅDFRI Gateway on which the device exists.
     * @param array   $deviceData Raw data as received from the TRÅDFRI Gateway.
     */
    public function __construct(Tradfri $tradfri, array $deviceData)
    {
        $this->tradfri = $tradfri;
        $this->deviceData = $deviceData;
    }

    /**
     * Returns the instance ID of the device
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->deviceData[static::INSTANCE_ID];
    }

    /**
     * Returns the name of the device
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->deviceData[static::NAME];
    }

    /**
     * Returns the name of the manufacturer
     *
     * @return string
     */
    public function getManufacturer(): string
    {
        return $this->deviceData[static::DEVICE][0];
    }

    /**
     * Returns the version of the devices firmware
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->deviceData[static::DEVICE][3];
    }

    /**
     * Returns the name of the product
     *
     * @return string
     */
    public function getProductName(): string
    {
        return $this->deviceData[static::DEVICE][1];
    }

    /**
     * Returns the timestamp on which this item is created
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return new \DateTime('@' . $this->deviceData[static::CREATED_AT]);
    }

    /**
     * Returns the timestamp on which this item has been last seen
     *
     * @return \DateTime
     */
    public function getLastSeen(): \DateTime
    {
        return new \DateTime('@' . $this->deviceData[static::LAST_SEEN]);
    }

    /**
     * Returns true if the device is reachable, false if not.
     *
     * @return boolean
     */
    public function isReachable(): bool
    {
        return (bool)$this->deviceData[static::REACHABILITY_STATE];
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array(
            'id'           => $this->getId(),
            'name'         => $this->getName(),
            'type'         => get_called_class(),
            'manufacturer' => $this->getManufacturer(),
            'productName'  => $this->getProductName(),
            'version'      => $this->getVersion(),
            'reachable'    => $this->isReachable(),
            'createdAt'    => $this->getCreatedAt()->format('c'),
            'lastSeen'     => $this->getLastSeen()->format('c'),
            //'raw'          => $this->deviceData,
        );
    }
}
