<?php

namespace Garrcomm\Tradfri\Model;

class TradfriPlug extends BaseTradfriDevice
{
    private const
        PLUG = 3312,
        ONOFF = 5850
    ;

    /**
     * Returns true if the plug is on, otherwise false.
     *
     * @return boolean
     */
    public function isOn(): bool
    {
        return (bool)$this->deviceData[static::PLUG][0][static::ONOFF];
    }

    /**
     * Turns on the device
     *
     * @return void
     */
    public function turnOn(): void
    {
        $this->tradfri->writeToDevice($this->getId(), [static::PLUG => [[
            static::ONOFF => 1,
        ]]]);
        $this->deviceData[static::PLUG][0][static::ONOFF] = 1;
    }

    /**
     * Turns off the device
     *
     * @return void
     */
    public function turnOff(): void
    {
        $this->tradfri->writeToDevice($this->getId(), [static::PLUG => [[
            static::ONOFF => 0,
        ]]]);
        $this->deviceData[static::PLUG][0][static::ONOFF] = 0;
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @return mixed data which can be serialized by json_encode, which is a value of any type other than a resource.
     *
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     */
    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), array(
            'on'      => $this->isOn(),
        ));
    }
}
