<?php

namespace Garrcomm\Tradfri\Service;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriBlind;
use Garrcomm\Tradfri\Model\TradfriGateway;
use Garrcomm\Tradfri\Model\TradfriGroup;
use Garrcomm\Tradfri\Model\TradfriLight;
use Garrcomm\Tradfri\Model\TradfriPlug;
use Garrcomm\Tradfri\Model\TradfriRemote;
use Garrcomm\Tradfri\Model\TradfriColorLight;
use Garrcomm\Tradfri\Model\TradfriScene;
use Garrcomm\Tradfri\Model\UnknownTradfriDevice;

class Tradfri
{
    /**
     * IP address of the TRÅDFRI Gateway
     *
     * @var string
     */
    private $ip;

    /**
     * Path to the coap-client executable
     *
     * @var string
     */
    private $coapClientPath;

    /**
     * Private shared key from the TRÅDFRI Gateway
     *
     * @var string
     */
    private $privateSharedKey;

    /**
     * Client identity for the TRÅDFRI Gateway (32 hexadecimal characters)
     *
     * @var string
     */
    private $clientIdentity;

    /**
     * Numeric paths defined
     */
    private const
        TYPE = 5750,
        QUERY_AUTH_PATH = 9063,
        CLIENT_IDENTITY_PROPOSED = 9090,
        NEW_PSK_BY_GW = 9091,
        QUERY_DEVICES = 15001,
        QUERY_GROUPS = 15004,
        QUERY_SCENE = 15005,
        QUERY_GATEWAY = 15011,
        GATEWAY_DETAILS = 15012
    ;

    /**
     * Cache for devices
     *
     * @var BaseTradfriDevice[]
     */
    private $deviceCache = array();

    /**
     * Cache for scenes
     *
     * @var TradfriScene[]
     */
    private $sceneCache = array();

    /**
     * Cache for groups
     *
     * @var TradfriGroup[]
     */
    private $groupCache = array();

    /**
     * Initializes a connection to a TRÅDFRI Gateway
     *
     * @param string $ip             IP address of the TRÅDFRI Gateway.
     * @param string $coapClientPath Path to the coap-client executable.
     */
    public function __construct(string $ip, string $coapClientPath)
    {
        // Validates the input
        if (!preg_match('/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/', $ip)) {
            throw new TradfriException(
                'The IPv4 address specified for the gateway is not correctly formatted',
                TradfriException::INVALID_IPV4_ADDRESS
            );
        }
        if (!file_exists($coapClientPath) || !$this->isExecutable($coapClientPath)) {
            throw new TradfriException(
                'The coap-client path could not be found or is not executable',
                TradfriException::INVALID_COAP_CLIENT
            );
        }

        $this->ip = $ip;
        $this->coapClientPath = $coapClientPath;
    }

    /**
     * Define the client identity (if you don't have one, use $this->authenticate() first)
     *
     * @param string $clientIdentity   Client identity for the TRÅDFRI Gateway (32 hexadecimal characters).
     * @param string $privateSharedKey Private shared key from the TRÅDFRI Gateway.
     *
     * @return void
     */
    public function setClientIdentity(string $clientIdentity, string $privateSharedKey): void
    {
        $this->clientIdentity = $clientIdentity;
        $this->privateSharedKey = $privateSharedKey;
    }

    /**
     * Returns a list of all known devices in the TRÅDFRI Gateway
     *
     * @return BaseTradfriDevice[]
     */
    public function listDevices(): array
    {
        $return = array();

        $deviceIds = $this->getQuery(static::QUERY_DEVICES);
        foreach ($deviceIds as $deviceId) {
            $return[$deviceId] = $this->getDevice($deviceId);
        }

        return $return;
    }

    /**
     * Returns information about the Gateway device
     *
     * @return TradfriGateway
     */
    public function getGateway(): TradfriGateway
    {
        return new TradfriGateway($this, $this->getQuery(static::QUERY_GATEWAY . '/' . static::GATEWAY_DETAILS));
    }

    /**
     * Get a specific device from the TRÅDFRI Gateway
     *
     * @param integer $deviceId Unique ID of the device.
     * @param boolean $refresh  Set to true to be sure data is up-to-date.
     *
     * @return BaseTradfriDevice
     */
    public function getDevice(int $deviceId, bool $refresh = false): BaseTradfriDevice
    {
        if ($refresh || !isset($this->deviceCache[$deviceId])) {
            try {
                $deviceData = $this->getQuery(static::QUERY_DEVICES . '/' . $deviceId);
            } catch (TradfriException $exception) {
                if ($exception->getCode() == TradfriException::COAP_SERVER_ERROR + 404) {
                    throw new TradfriException('Device not found', TradfriException::ITEM_NOT_FOUND);
                }
                throw $exception;
            }
            switch ($deviceData[static::TYPE]) {
                case 0: // Switch / Remote control / Shortcut button
                case 4: // Motion sensor
                case 8: // Sound control
                    $this->deviceCache[$deviceId] = new TradfriRemote($this, $deviceData);
                    break;
                case 2: // Light
                    if (isset($deviceData[3311][0][5706])) {
                        $this->deviceCache[$deviceId] = new TradfriColorLight($this, $deviceData);
                    } else {
                        $this->deviceCache[$deviceId] = new TradfriLight($this, $deviceData);
                    }
                    break;
                case 3: // Plug
                    $this->deviceCache[$deviceId] = new TradfriPlug($this, $deviceData);
                    break;
                case 7: // Blind
                    $this->deviceCache[$deviceId] = new TradfriBlind($this, $deviceData);
                    break;
                case 6: // Signal repeater
                default:
                    $this->deviceCache[$deviceId] = new UnknownTradfriDevice($this, $deviceData);
            }
        }
        return $this->deviceCache[$deviceId];
    }

    /**
     * Returns a list of all known groups in the TRÅDFRI Gateway
     *
     * @return TradfriGroup[]
     */
    public function listGroups(): array
    {
        $return = array();

        $groupIds = $this->getQuery(static::QUERY_GROUPS);
        foreach ($groupIds as $groupId) {
            $return[$groupId] = $this->getGroup($groupId);
        }

        return $return;
    }

    /**
     * Returns a list of all known scenes in the TRÅDFRI Gateway
     *
     * @return TradfriScene[]
     */
    public function listScenes(): array
    {
        $return = array();

        $sceneGroupIds = $this->getQuery(static::QUERY_SCENE);
        foreach ($sceneGroupIds as $groupId) {
            $sceneIds = $this->getQuery(static::QUERY_SCENE . '/' . $groupId);
            foreach ($sceneIds as $sceneId) {
                $return[$sceneId] = $this->getSceneByGroup($groupId, $sceneId);
            }
        }

        return $return;
    }

    /**
     * Get a specific scene from the TRÅDFRI Gateway
     *
     * @param integer $groupId Scenes are linked to device groups; but apparently all scenes are in the SuperGroup.
     * @param integer $sceneId Unique ID of the scene.
     *
     * @return TradfriScene
     */
    private function getSceneByGroup(int $groupId, int $sceneId): TradfriScene
    {
        if (!isset($this->sceneCache[$sceneId])) {
            $sceneData = $this->getQuery(static::QUERY_SCENE . '/' . $groupId . '/' . $sceneId);
            $sceneData['_groupId'] = $groupId;
            $this->sceneCache[$sceneId] = new TradfriScene($this, $sceneData);
        }
        return $this->sceneCache[$sceneId];
    }

    /**
     * Get a specific scene from the TRÅDFRI Gateway
     *
     * @param integer $sceneId Unique ID of the scene.
     * @param boolean $refresh Set to true to be sure data is up-to-date.
     *
     * @return TradfriScene
     */
    public function getScene(int $sceneId, bool $refresh = false): TradfriScene
    {
        if ($refresh && isset($this->sceneCache[$sceneId])) {
            unset($this->sceneCache[$sceneId]);
        }
        if (!isset($this->sceneCache[$sceneId])) {
            $this->listScenes(); // Fetches all scenes for now; we need the group ID to access them
        }
        if (!isset($this->sceneCache[$sceneId])) {
            throw new TradfriException('Scene not found', TradfriException::ITEM_NOT_FOUND);
        }

        return $this->sceneCache[$sceneId];
    }

    /**
     * Get a specific group from the TRÅDFRI Gateway
     *
     * @param integer $groupId Unique ID of the group.
     * @param boolean $refresh Set to true to be sure data is up-to-date.
     *
     * @return TradfriGroup
     */
    public function getGroup(int $groupId, bool $refresh = false): TradfriGroup
    {
        if ($refresh || !isset($this->groupCache[$groupId])) {
            try {
                $groupData = $this->getQuery(static::QUERY_GROUPS . '/' . $groupId);
            } catch (TradfriException $exception) {
                if ($exception->getCode() == TradfriException::COAP_SERVER_ERROR + 404) {
                    throw new TradfriException('Group not found', TradfriException::ITEM_NOT_FOUND);
                }
                throw $exception;
            }
            $this->groupCache[$groupId] = new TradfriGroup(
                $this,
                $groupData
            );
        }
        return $this->groupCache[$groupId];
    }

    /**
     * Authenticates against the TRÅDFRI Gateway and returns a clientIdentity and privateSharedKey.
     *
     * @param string $securityCode The security code that's written on the back of the TRÅDFRI Gateway.
     *
     * @return string[] An array with two keys: clientIdentity and privateSharedKey
     */
    public function authenticate(string $securityCode): array
    {
        // Generate 32-byte long hex string (so binary 16 byte long)
        $clientIdHex = '';
        for ($iterator = 0; $iterator < 16; ++$iterator) {
            $clientIdHex .= str_pad(dechex(rand(0, 255)), 2, '0', STR_PAD_LEFT);
        }

        // Post the authorize request with the security code as PSK and a generic identity
        try {
            $result = $this->postQuery(static::QUERY_GATEWAY . '/' . static::QUERY_AUTH_PATH, [
                static::CLIENT_IDENTITY_PROPOSED => $clientIdHex
            ], [
                'ClientIdentity' => 'Client_identity',
                'PrivateSharedKey' => $securityCode,
            ]);
        } catch (\JsonException $exception) {
            throw new TradfriException('Could not authenticate', TradfriException::AUTHENTICATION_FAILED);
        }
        if ($result === null) {
            throw new TradfriException('Could not authenticate', TradfriException::AUTHENTICATION_FAILED);
        }
        $this->clientIdentity = $clientIdHex;
        $this->privateSharedKey = $result[static::NEW_PSK_BY_GW];

        return [
            'clientIdentity'   => $this->clientIdentity,
            'privateSharedKey' => $this->privateSharedKey,
        ];
    }

    /**
     * Posts data to a specific end-point
     *
     * @param string $path    The endpoint.
     * @param mixed  $data    The data.
     * @param array  $options Additional options.
     *
     * @return mixed
     */
    private function postQuery(string $path, $data, array $options = array())
    {
        // Fetch default options
        if (!isset($options['PrivateSharedKey'])) {
            $options['PrivateSharedKey'] = $this->privateSharedKey;
        }
        if (!isset($options['ClientIdentity'])) {
            $options['ClientIdentity'] = $this->clientIdentity;
        }
        if (!isset($options['method'])) {
            $options['method'] = 'post';
        }

        // Execute command
        return $this->exec($this->escapeshellcmd($this->coapClientPath) . ' -m ' . escapeshellarg($options['method'])
            . ' -u ' . escapeshellarg($options['ClientIdentity'])
            . ' -k ' . escapeshellarg($options['PrivateSharedKey'])
            . ' -B 5'
            . ($data !== null ? ' -e ' . rawurlencode(json_encode($data)) : '')
            . ' ' . escapeshellarg('coaps://' . $this->ip . ':5684/' . $path));
    }

    /**
     * The same as escapeshellcmd but more Windows friendly (especially when there are spaces in the command name)
     *
     * @param string $arg The command that will be escaped.
     *
     * @return string
     */
    private function escapeshellcmd(string $arg): string
    {
        if (PHP_OS != 'WINNT' || strpos($arg, ' ') === false) {
            return escapeshellcmd($arg);
        }
        return 'call ' . escapeshellarg($arg);
    }

    /**
     * The same as is_executable but more Windows friendly (especially for batch files and installers)
     *
     * @param string $filename Path to the file.
     *
     * @return string
     */
    private function isExecutable(string $filename): string
    {
        if (PHP_OS != 'WINNT' || pathinfo($filename, PATHINFO_EXTENSION) == 'exe') {
            return is_executable($filename);
        }
        return in_array(pathinfo($filename, PATHINFO_EXTENSION), ['bat', 'cmd', 'msi']);
    }

    /**
     * Gets data from a specific end-point
     *
     * @param string $path    The endpoint.
     * @param array  $options Additional options.
     *
     * @return mixed
     */
    private function getQuery(string $path, array $options = array())
    {
        $data = $this->postQuery($path, null, array_merge($options, ['method' => 'get']));
        if ($data === null) {
            throw new TradfriException('Can\'t connect to the gateway', TradfriException::CONNECTION_FAILED);
        }
        return $data;
    }

    /**
     * Writes specific data to a device
     *
     * @param integer $deviceId  Device instance ID.
     * @param array   $newValues Data to write.
     *
     * @return void
     */
    public function writeToDevice(int $deviceId, array $newValues): void
    {
        $this->postQuery(static::QUERY_DEVICES . '/' . $deviceId, $newValues, ['method' => 'put']);
    }

    /**
     * Writes specific data to a group
     *
     * @param integer $groupId   Group instance ID.
     * @param array   $newValues Data to write.
     *
     * @return void
     */
    public function writeToGroup(int $groupId, array $newValues): void
    {
        $this->postQuery(static::QUERY_GROUPS . '/' . $groupId, $newValues, ['method' => 'put']);
    }

    /**
     * Executes a shell command, searches for json in the output and returns the json decoded
     *
     * @param string $command Command to execute.
     *
     * @return mixed
     */
    private function exec(string $command)
    {
        // Creates a process, outputs STDOUT (1) and STDERR (2) to a pipe
        $proc = proc_open($command, [
            1 => ['pipe', 'w'],
            2 => ['pipe', 'w'],
        ], $pipes);

        // Read and close pipes
        $stdout = rtrim(stream_get_contents($pipes[1]), PHP_EOL);
        $stderr = rtrim(stream_get_contents($pipes[2]), PHP_EOL);
        fclose($pipes[1]);
        fclose($pipes[2]);

        // Fetches the result code
        $resultCode = proc_close($proc);
        if ($resultCode != 0) {
            throw new TradfriException(
                'Command exited with an error',
                TradfriException::COAP_CLIENT_ERROR + $resultCode,
                ['command' => $command, 'stdout' => $stdout, 'stderr' => $stderr]
            );
        }

        // Did we get an error code?
        foreach (explode(PHP_EOL, $stderr) as $line) {
            if (substr($line, 0, 9) == 'v:1 t:CON') {
                continue;
            }
            if (preg_match('/^[0-9]{1}\.[0-9]{2}/', $line, $matches)) {
                $errorCode = trim(str_replace('.', '', $matches[0]));
                throw new TradfriException(
                    'Command exited with an error',
                    TradfriException::COAP_SERVER_ERROR + $errorCode,
                    ['command' => $command, 'stdout' => $stdout, 'stderr' => $stderr]
                );
            }
        }

        // The response may be empty
        if (empty($stdout)) {
            return null;
        }

        return json_decode(rtrim($stdout, PHP_EOL), true, 512, JSON_THROW_ON_ERROR);
    }
}
