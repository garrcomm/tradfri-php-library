<?php

namespace Tests\Model;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriLight;
use Garrcomm\Tradfri\Model\TradfriColorLight;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriColorLightTest extends TestCase
{
    /**
     * Instance ID of one of the multicolor lights
     */
    private const TEST_MULTICOLOR_LIGHT = 65558;

    /**
     * Tests all simple getters; we should be able to pass this without throwable.
     *
     * @return void
     */
    public function testSimpleGetters(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp (can be done from cache)
        $lamp = $tradfri->getDevice(static::TEST_MULTICOLOR_LIGHT); /* @var $lamp TradfriColorLight */

        // BaseTradfriDevice
        $this->assertInstanceOf(BaseTradfriDevice::class, $lamp);
        $this->assertEquals(static::TEST_MULTICOLOR_LIGHT, $lamp->getId());
        $this->assertIsString($lamp->getName());
        $this->assertIsString($lamp->getManufacturer());
        $this->assertIsString($lamp->getProductName());
        $this->assertMatchesRegularExpression('/^[0-9]+\.[0-9]+\.[0-9]+$/', $lamp->getVersion());
        $this->assertInstanceOf(\DateTimeInterface::class, $lamp->getCreatedAt());
        $this->assertInstanceOf(\DateTimeInterface::class, $lamp->getLastSeen());
        $this->assertIsBool($lamp->isReachable());
        $this->assertJson(json_encode($lamp));

        // TradfriLight
        $this->assertInstanceOf(TradfriLight::class, $lamp);
        $this->assertIsBool($lamp->isOn());
        $this->assertLessThan(255, $lamp->getBrightness());
        $this->assertGreaterThanOrEqual(0, $lamp->getBrightness());

        // TradfriColorLight
        $this->assertInstanceOf(TradfriColorLight::class, $lamp);
        $this->assertLessThanOrEqual(0xffff, $lamp->getColorX());
        $this->assertGreaterThanOrEqual(0, $lamp->getColorX());
        $this->assertLessThanOrEqual(0xffff, $lamp->getColorY());
        $this->assertGreaterThanOrEqual(0, $lamp->getColorY());
        $this->assertMatchesRegularExpression('/^[0-9a-f]{6}$/', $lamp->getRgbColor());

        // Hard refresh (not from cache) should result in the same device
        $hardRefresh = $tradfri->getDevice(static::TEST_MULTICOLOR_LIGHT, true);
        $this->assertEquals($lamp, $hardRefresh);
    }

    /**
     * Test setting a color by it's RGB value
     *
     * @return void
     */
    public function testSetRgbColor(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp (can be done from cache)
        $lamp = $tradfri->getDevice(static::TEST_MULTICOLOR_LIGHT); /* @var $lamp TradfriColorLight */

        foreach (['4a418a', 'd6e44b', 'e491af'] as $color) {
            $lamp->setRgbColor($color);
            $this->assertEquals($color, $lamp->getRgbColor());
        }
    }

    /**
     * Tests setRgbColor with a non-hex value
     *
     * @return void
     */
    public function testSetRgbColorInvalidHex(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp (can be done from cache)
        $lamp = $tradfri->getDevice(static::TEST_MULTICOLOR_LIGHT); /* @var $lamp TradfriColorLight */
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::VALUE_OUT_OF_RANGE);
        $lamp->setRgbColor('invalid');
    }

    /**
     * Tests setRgbColor with a non-supported value
     *
     * @return void
     */
    public function testSetRgbColorInvalidNotSupported(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp (can be done from cache)
        $lamp = $tradfri->getDevice(static::TEST_MULTICOLOR_LIGHT); /* @var $lamp TradfriColorLight */
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::VALUE_OUT_OF_RANGE);
        $lamp->setRgbColor('00ff00');
    }

    /**
     * Tests the setXyColor() method.
     *
     * @return void
     */
    public function testSetXyColor(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp (can be done from cache)
        $lamp = $tradfri->getDevice(static::TEST_MULTICOLOR_LIGHT); /* @var $lamp TradfriColorLight */

        foreach ([[11469, 3277], [29491, 18350], [20316, 8520]] as $color) {
            $lamp->setXyColor($color[0], $color[1]);
            $this->assertEquals($color[0], $lamp->getColorX());
            $this->assertEquals($color[1], $lamp->getColorY());
        }
    }

    /**
     * Tests the setXyColor() method with out-of-range values.
     *
     * @return void
     */
    public function testSetXyColorOutOfRange(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp (can be done from cache)
        $lamp = $tradfri->getDevice(static::TEST_MULTICOLOR_LIGHT); /* @var $lamp TradfriColorLight */

        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::VALUE_OUT_OF_RANGE);
        $lamp->setXyColor(100000, -5);
    }
}
