<?php

namespace Tests\Model;

use Garrcomm\Tradfri\Model\TradfriGateway;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriGatewayTest extends TestCase
{
    /**
     * Tests the getGateway() method
     *
     * @return void
     */
    public function testGetGateway(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch the Gateway object
        $gateway = $tradfri->getGateway();
        // Do we get a Gateway object?
        $this->assertInstanceOf(TradfriGateway::class, $gateway);
        // Is the version correctly formatted?
        $this->assertMatchesRegularExpression('/^[0-9]+\.[0-9]+\.[0-9]+$/', $gateway->getVersion());
        // Can we encode the object in JSON?
        $this->assertJson(json_encode($gateway));
    }
}
