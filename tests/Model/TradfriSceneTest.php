<?php

namespace Tests\Model;

use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriSceneTest extends TestCase
{
    /**
     * Instance ID of one of the scenes
     */
    private const TEST_SCENE = 196632;

    /**
     * Tests all simple getters; we should be able to pass this without throwable.
     *
     * @return void
     */
    public function testSimpleGetters(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch scene (can be done from cache)
        $scene = $tradfri->getScene(static::TEST_SCENE);
        $this->assertEquals(static::TEST_SCENE, $scene->getId());
        $this->assertIsString($scene->getName());
        $this->assertIsInt($scene->getIconIndex());
        $this->assertInstanceOf(\DateTimeInterface::class, $scene->getCreatedAt());
        $this->assertJson(json_encode($scene));

        // Hard refresh (not from cache) should result in the same scene
        $hardRefresh = $tradfri->getScene(static::TEST_SCENE, true);
        $this->assertEquals($scene, $hardRefresh);
    }

    /**
     * Tests the Execute method; execute() could result in an TradfriException, but should not.
     *
     * @return void
     */
    public function testExecute(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        $scene = $tradfri->getScene(static::TEST_SCENE);
        $scene->execute();

        $this->assertTrue(true);
    }
}
