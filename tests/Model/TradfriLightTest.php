<?php

namespace Tests\Model;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriLight;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriLightTest extends TestCase
{
    /**
     * Instance ID of one of the lights
     */
    private const TEST_LIGHT = 65557;

    /**
     * Tests all simple getters; we should be able to pass this without throwable.
     *
     * @return void
     */
    public function testSimpleGetters(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp (can be done from cache)
        $lamp = $tradfri->getDevice(static::TEST_LIGHT); /* @var $lamp TradfriLight */

        // BaseTradfriDevice
        $this->assertInstanceOf(BaseTradfriDevice::class, $lamp);
        $this->assertEquals(static::TEST_LIGHT, $lamp->getId());
        $this->assertIsString($lamp->getName());
        $this->assertIsString($lamp->getManufacturer());
        $this->assertIsString($lamp->getProductName());
        $this->assertMatchesRegularExpression('/^[0-9]+\.[0-9]+\.[0-9]+$/', $lamp->getVersion());
        $this->assertInstanceOf(\DateTimeInterface::class, $lamp->getCreatedAt());
        $this->assertInstanceOf(\DateTimeInterface::class, $lamp->getLastSeen());
        $this->assertIsBool($lamp->isReachable());
        $this->assertJson(json_encode($lamp));

        // TradfriLight
        $this->assertInstanceOf(TradfriLight::class, $lamp);
        $this->assertIsBool($lamp->isOn());
        $this->assertLessThan(255, $lamp->getBrightness());
        $this->assertGreaterThanOrEqual(0, $lamp->getBrightness());

        // Hard refresh (not from cache) should result in the same device
        $hardRefresh = $tradfri->getDevice(static::TEST_LIGHT, true);
        $this->assertEquals($lamp, $hardRefresh);
    }

    /**
     * Tests the turnOn and turnOff methods.
     *
     * @return void
     */
    public function testTurnOnOff(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp
        $lamp = $tradfri->getDevice(static::TEST_LIGHT); /* @var $lamp TradfriLight */

        // Turn on
        $lamp->turnOn();
        $this->assertTrue($lamp->isOn());

        // Turn off
        $lamp->turnOff();
        $this->assertFalse($lamp->isOn());

        // Turn on again
        $lamp->turnOn();
        $this->assertTrue($lamp->isOn());
    }

    /**
     * Tests if we get an exception with a negative brightness value
     *
     * @return void
     */
    public function testSetBrightnessTooLow(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp
        $lamp = $tradfri->getDevice(static::TEST_LIGHT); /* @var $lamp TradfriLight */
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::VALUE_OUT_OF_RANGE);
        $lamp->setBrightness(-1);
    }

    /**
     * Tests if we get an exception with a brightness value that's too high
     *
     * @return void
     */
    public function testSetBrightnessTooHigh(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp
        $lamp = $tradfri->getDevice(static::TEST_LIGHT); /* @var $lamp TradfriLight */
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::VALUE_OUT_OF_RANGE);
        $lamp->setBrightness(255);
    }

    /**
     * Tests a few brightness values
     *
     * @return void
     */
    public function testSetBrightness(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch lamp
        $lamp = $tradfri->getDevice(static::TEST_LIGHT); /* @var $lamp TradfriLight */
        foreach ([0, 50, 100, 150, 200, 250] as $brightnessValue) {
            $lamp->setBrightness($brightnessValue);
            $this->assertEquals($brightnessValue, $lamp->getBrightness());
        }
    }
}
