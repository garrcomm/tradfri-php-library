<?php

namespace Tests\Model;

use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriPlug;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriPlugTest extends TestCase
{
    /**
     * Instance ID of one of the plugs
     */
    private const TEST_PLUG = 65553;

    /**
     * Tests all simple getters; we should be able to pass this without throwable.
     *
     * @return void
     */
    public function testSimpleGetters(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch plug (can be done from cache)
        $plug = $tradfri->getDevice(static::TEST_PLUG); /* @var $plug TradfriPlug */

        // BaseTradfriDevice
        $this->assertInstanceOf(BaseTradfriDevice::class, $plug);
        $this->assertEquals(static::TEST_PLUG, $plug->getId());
        $this->assertIsString($plug->getName());
        $this->assertIsString($plug->getManufacturer());
        $this->assertIsString($plug->getProductName());
        $this->assertMatchesRegularExpression('/^[0-9]+\.[0-9]+\.[0-9]+$/', $plug->getVersion());
        $this->assertInstanceOf(\DateTimeInterface::class, $plug->getCreatedAt());
        $this->assertInstanceOf(\DateTimeInterface::class, $plug->getLastSeen());
        $this->assertIsBool($plug->isReachable());
        $this->assertJson(json_encode($plug));

        // TradfriPlug
        $this->assertInstanceOf(TradfriPlug::class, $plug);
        $this->assertIsBool($plug->isOn());

        // Hard refresh (not from cache) should result in the same device
        $hardRefresh = $tradfri->getDevice(static::TEST_PLUG, true);
        $this->assertEquals($plug, $hardRefresh);
    }

    /**
     * Tests the turnOn and turnOff methods.
     *
     * @return void
     */
    public function testTurnOnOff(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch plug
        $plug = $tradfri->getDevice(static::TEST_PLUG); /* @var $plug TradfriPlug */

        // Turn on
        $plug->turnOn();
        $this->assertTrue($plug->isOn());

        // Turn off
        $plug->turnOff();
        $this->assertFalse($plug->isOn());

        // Turn on again
        $plug->turnOn();
        $this->assertTrue($plug->isOn());
    }
}
