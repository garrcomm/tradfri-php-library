<?php

namespace Tests\Model;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriLight;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriGroupTest extends TestCase
{
    /**
     * Instance ID of one of the groups
     */
    private const TEST_GROUP = 131077;

    /**
     * Tests all simple getters; we should be able to pass this without throwable.
     *
     * @return void
     */
    public function testSimpleGetters(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch group (can be done from cache)
        $group = $tradfri->getGroup(static::TEST_GROUP);
        $this->assertEquals(static::TEST_GROUP, $group->getId());
        $this->assertIsString($group->getName());
        $this->assertInstanceOf(\DateTimeInterface::class, $group->getCreatedAt());
        $this->assertJson(json_encode($group));

        // Validate devices result
        $devices = $group->getDevices();
        $this->assertIsArray($devices);
        foreach ($devices as $device) {
            $this->assertInstanceOf(BaseTradfriDevice::class, $device);
        }

        // Hard refresh (not from cache) should result in the same group
        $hardRefresh = $tradfri->getGroup(static::TEST_GROUP, true);
        $this->assertEquals($group, $hardRefresh);
    }

    /**
     * Tests the turnOn and turnOff methods.
     *
     * @return void
     */
    public function testTurnOnOff(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch group
        $group = $tradfri->getGroup(static::TEST_GROUP);

        // Turn on
        $group->turnOn();
        $this->assertTrue(true); // No exception

        // Turn off
        $group->turnOff();
        $this->assertTrue(true); // No exception
    }

    /**
     * Tests if we get an exception with a negative brightness value
     *
     * @return void
     */
    public function testSetBrightnessTooLow(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch group
        $group = $tradfri->getGroup(static::TEST_GROUP);
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::VALUE_OUT_OF_RANGE);
        $group->setBrightness(-1);
    }

    /**
     * Tests if we get an exception with a brightness value that's too high
     *
     * @return void
     */
    public function testSetBrightnessTooHigh(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch group
        $group = $tradfri->getGroup(static::TEST_GROUP);
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::VALUE_OUT_OF_RANGE);
        $group->setBrightness(255);
    }

    /**
     * Tests a few brightness values
     *
     * @return void
     */
    public function testSetBrightness(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch group
        $group = $tradfri->getGroup(static::TEST_GROUP);
        foreach ([0, 50, 100, 150, 200, 250] as $brightnessValue) {
            $group->setBrightness($brightnessValue);
            $this->assertTrue(true); // No exception
        }
    }
}
