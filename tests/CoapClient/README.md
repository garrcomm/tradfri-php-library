# coap-client simulator

This folder contains a simulator for the coap-client dependency of this project.
There are two useful applications for this script:

1. Running the unit tests (for this reason I started building this simulator).
2. Building/testing/maintaining a TRÅDFRI app when no gateway can be found in the network.

This coap-client only accepts `coaps://127.0.0.1:5684/` as base URL.
Any other URL will result in the same error as the original client would return when the remote host is unreachable.

This coap-client returns a footprint of my own TRÅDFRI network at home. It's also stateless;
when you turn on a device, it will return a valid response, but won't remember that the device actually has been turned on.
This way, behaviour for unit tests is consistent.

The behaviour is also not 100% as if it's the real coap-client, but it'll do for testing/debugging purposes.

## Testing authentication

This coap-client expects the following credentials:

* Without PSK: `-u Client_identity -k valid-security-code`
* With a PSK: `-u [any-32-bit-hexadecimal-string] -k secretPrivateSharedKey`

If you specify anything else, you'll get an error as if it was on a real gateway.
