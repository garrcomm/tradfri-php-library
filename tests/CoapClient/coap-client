#!/usr/bin/env php
<?php
/**
 * This script is a fictive coap-client executable.
 */

// Don't use an autoloader; that makes this script less stand-alone
require_once __DIR__ . '/AbstractTestResult.php';
require_once __DIR__ . '/BaseTestResult.php';
require_once __DIR__ . '/Post15011.php';
require_once __DIR__ . '/Put15001.php';
require_once __DIR__ . '/Put15004.php';

// Parses the command line arguments
unset($argv[0]); // Remove the command line command itself
$parameters = array('--' => null, 'B' => null, 'm'  => null, 'u'  => null, 'k'  => null, 'e'  => null);
$current_parameter = '--';
foreach ($argv as $argument) {
    if (substr($argument, 0, 1) == '-') {
        $current_parameter = substr($argument, 1);
        if (!array_key_exists($current_parameter, $parameters)) {
            fputs(STDERR, 'Invalid parameter: "' . $current_parameter . '"' . PHP_EOL);
            exit(1);
        } elseif (isset($parameters[$current_parameter])) {
            fputs(STDERR, 'Duplicate parameter: "' . $current_parameter . '"' . PHP_EOL);
            exit(1);
        }
    } else {
        $parameters[$current_parameter] = $argument;
        $current_parameter = '--';
    }
}
// Default values
if (!isset($parameters['m'])) {
    $parameters['m'] = 'get';
}
if (!isset($parameters['B'])) {
    $parameters['B'] = 90;
}

// Show help page
if (count($argv) == 0) {
    fputs(STDERR, 'coap-client v4.1.2 -- a small CoAP implementation' . PHP_EOL);
    fputs(STDERR, '(c) 2010-2015 Olaf Bergmann <bergmann@tzi.org>' . PHP_EOL);
    fputs(STDERR, '' . PHP_EOL);
    fputs(STDERR, 'usage: coap-client [-A type...] [-t type] [-b [num,]size] [-B seconds] [-e text]' . PHP_EOL);
    fputs(STDERR, '                [-m method] [-N] [-o file] [-P addr[:port]] [-p port]' . PHP_EOL);
    fputs(STDERR, '                [-s duration] [-O num,text] [-T string] [-v num] [-a addr]' . PHP_EOL);
    fputs(STDERR, '' . PHP_EOL);
    fputs(STDERR, '                [-u user] [-k key] URI' . PHP_EOL);
    fputs(STDERR, '' . PHP_EOL);
    fputs(STDERR, '        URI can be an absolute or relative coap URI,' . PHP_EOL);
    fputs(STDERR, '        -a addr the local interface address to use' . PHP_EOL);
    fputs(STDERR, '        -A type...      accepted media types as comma-separated list of' . PHP_EOL);
    fputs(STDERR, '                        symbolic or numeric values' . PHP_EOL);
    fputs(STDERR, '        -t type         content format for given resource for PUT/POST' . PHP_EOL);
    fputs(STDERR, '        -b [num,]size   block size to be used in GET/PUT/POST requests' . PHP_EOL);
    fputs(STDERR, '                        (value must be a multiple of 16 not larger than 1024)' . PHP_EOL);
    fputs(STDERR, '                        If num is present, the request chain will start at' . PHP_EOL);
    fputs(STDERR, '                        block num' . PHP_EOL);
    fputs(STDERR, '        -B seconds      break operation after waiting given seconds' . PHP_EOL);
    fputs(STDERR, '                        (default is 90)' . PHP_EOL);
    fputs(STDERR, '        -e text         include text as payload (use percent-encoding for' . PHP_EOL);
    fputs(STDERR, '                        non-ASCII characters)' . PHP_EOL);
    fputs(STDERR, '        -f file         file to send with PUT/POST (use \'-\' for STDIN)' . PHP_EOL);
    fputs(STDERR, '        -k key          Pre-shared key for the specified user. This argument' . PHP_EOL);
    fputs(STDERR, '                        requires DTLS with PSK to be available.' . PHP_EOL);
    fputs(STDERR, '        -m method       request method (get|put|post|delete), default is \'get\'' . PHP_EOL);
    fputs(STDERR, '        -N              send NON-confirmable message' . PHP_EOL);
    fputs(STDERR, '        -o file         output received data to this file (use \'-\' for STDOUT)' . PHP_EOL);
    fputs(STDERR, '        -p port         listen on specified port' . PHP_EOL);
    fputs(STDERR, '        -s duration     subscribe for given duration [s]' . PHP_EOL);
    fputs(STDERR, '        -u user         user identity for pre-shared key mode. This argument' . PHP_EOL);
    fputs(STDERR, '                        requires DTLS with PSK to be available.' . PHP_EOL);
    fputs(STDERR, '        -v num          verbosity level (default: 3)' . PHP_EOL);
    fputs(STDERR, '        -O num,text     add option num with contents text to request' . PHP_EOL);
    fputs(STDERR, '        -P addr[:port]  use proxy (automatically adds Proxy-Uri option to' . PHP_EOL);
    fputs(STDERR, '                        request)' . PHP_EOL);
    fputs(STDERR, '        -T token        include specified token' . PHP_EOL);
    fputs(STDERR, '' . PHP_EOL);
    fputs(STDERR, 'examples:' . PHP_EOL);
    fputs(STDERR, '        coap-client -m get coap://[::1]/' . PHP_EOL);
    fputs(STDERR, '        coap-client -m get coap://[::1]/.well-known/core' . PHP_EOL);
    fputs(STDERR, '        coap-client -m get -T cafe coap://[::1]/time' . PHP_EOL);
    fputs(STDERR, '        echo 1000 | coap-client -m put -T cafe coap://[::1]/time -f -' . PHP_EOL);
    fputs(STDERR, '' . PHP_EOL);
    exit(1);
}

// Get the request method
$method = strtolower($parameters['m']);
if (!in_array($method, ['get', 'put', 'post', 'delete'])) {
    fputs(STDERR, 'Invalid method: "' . $method . '"' . PHP_EOL);
    exit(1);
}

// Parses the URL
if (!isset($parameters['--'])) {
    fputs(STDERR, 'This is a coap-client simulator.' . PHP_EOL);
    fputs(STDERR, 'This simulator can be useful for unit tests or debugging a coap depending application.' . PHP_EOL . PHP_EOL);
    exit(1);
}
if (substr($parameters['--'], 0, 23) !== 'coaps://127.0.0.1:5684/') {
    fputs(STDERR, 'getaddrinfo: Temporary failure in name resolution' . PHP_EOL . 'failed to resolve address' . PHP_EOL);
    exit(255);
}

// Send request to stderr
fputs(STDERR, 'v:1 t:CON c:' . strtoupper($method) . ' i:' . dechex(rand(0x1000, 0xffff)) . ' {} [ ]'. PHP_EOL);

// Can we find a test
$path = explode('/', ltrim(parse_url($parameters['--'], PHP_URL_PATH), '/'));
if (!preg_match('/^[0-9]+$/', $path[0])) {
    fputs(STDERR, '4.04 Not Found' . PHP_EOL);
    exit(0);
}

// Test class name
$testClassName = '\\Tests\\CoapClient\\' . ucfirst($method . $path[0]);
// When the test can't be found, include the generic one
if (!class_exists($testClassName)) {
    $testClassName = \Tests\CoapClient\BaseTestResult::class;
}

$testClass = new $testClassName($path, $parameters); /* @var $testClass \Tests\CoapClient\AbstractTestResult */
if (!$testClass->passAuthentication()) {
    fputs(STDOUT, date('M d H:i:s') . ' WARN error in check_server_hellodone err: -592' . PHP_EOL);
    fputs(STDOUT, date('M d H:i:s') . ' WARN error while handling handshake packet' . PHP_EOL);
    fputs(STDERR, date('M d H:i:s') . ' WARN cannot set psk -- buffer too small' . PHP_EOL);
    fputs(STDERR, date('M d H:i:s') . ' CRIT no psk key for session available' . PHP_EOL);
    exit(0);
}
exit($testClass->run());
