<?php

namespace Tests\CoapClient;

class Post15011 extends AbstractTestResult
{
    /**
     * Runs the test result, returns an exit code (0 = success, 1 = failure)
     *
     * @return integer
     */
    public function run(): int
    {
        if ($this->path[1] == '9063') {
            return $this->runAuthenticate();
        }

        $this->stderr('4.04 Not Found');
        return 0;
    }

    /**
     * Runs the test result for authenticate requests, returns an exit code (0 = success, 1 = failure)
     *
     * @return integer
     */
    private function runAuthenticate(): int
    {
        // Validate input
        $data = json_decode(rawurldecode($this->parameters['e']), true);
        if (!isset($data['9090']) || !preg_match('/^[0-9a-f]{32}$/i', $data['9090'])) {
            $this->stderr('4.00');
            return 0;
        }

        // Output a new private shared key
        $this->stdout(json_encode([
            '9091' => 'secretPrivateSharedKey'
        ]));
        return 0;
    }

    /**
     * This method executed authentication. Authentication can be overwritten for specific tests.
     *
     * @return boolean
     */
    public function passAuthentication(): bool
    {
        if ($this->parameters['u'] !== 'Client_identity' || $this->parameters['k'] !== 'valid-security-code') {
            return false;
        }
        return true;
    }
}
