<?php

namespace Tests\CoapClient;

abstract class AbstractTestResult
{
    /**
     * The requested path
     *
     * @var array
     */
    protected $path;
    /**
     * Requested parameters
     *
     * @var array
     */
    protected $parameters;

    /**
     * Initializes the test result for request "POST /15011"
     *
     * @param array $path       The requested path.
     * @param array $parameters Requested parameters.
     */
    public function __construct(array $path, array $parameters)
    {
        $this->path = $path;
        $this->parameters = $parameters;
    }

    /**
     * Runs the test result, returns an exit code (0 = success, 1 = failure)
     *
     * @return integer
     */
    abstract public function run(): int;

    /**
     * Outputs text to STDOUT
     *
     * @param string  $text    Text to output.
     * @param boolean $newLine Set to false to skip the newline character on the end.
     *
     * @return void
     */
    protected function stdout(string $text, bool $newLine = true): void
    {
        fputs(STDOUT, $text . ($newLine ? PHP_EOL : ''));
    }

    /**
     * Outputs text to STDERR
     *
     * @param string  $text    Text to output.
     * @param boolean $newLine Set to false to skip the newline character on the end.
     *
     * @return void
     */
    protected function stderr(string $text, bool $newLine = true): void
    {
        fputs(STDERR, $text . ($newLine ? PHP_EOL : ''));
    }

    /**
     * This method executed authentication. Authentication can be overwritten for specific tests.
     *
     * @return boolean
     */
    public function passAuthentication(): bool
    {
        if (
            !preg_match('/^[0-9a-f]{32}$/i', $this->parameters['u'])
            || $this->parameters['k'] !== 'secretPrivateSharedKey'
        ) {
            return false;
        }
        return true;
    }
}
