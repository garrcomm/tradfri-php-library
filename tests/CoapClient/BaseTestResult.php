<?php

namespace Tests\CoapClient;

class BaseTestResult extends AbstractTestResult
{
    /**
     * Runs the test result, returns an exit code (0 = success, 1 = failure)
     *
     * @return integer
     */
    public function run(): int
    {
        if ($this->parameters['m'] == 'get' && !$this->parameters['e']) {
            $baseAnswers = json_decode(file_get_contents(__DIR__ . '/BaseAnswers.json'), true);
            $path = implode('/', $this->path);
            if (isset($baseAnswers[$path])) {
                // Return stdout example
                $this->stdout($baseAnswers[$path]['stdout']);

                // Remove the protocol line from the stderr example
                $stderr = explode(PHP_EOL, rtrim($baseAnswers[$path]['stderr'], PHP_EOL));
                foreach ($stderr as $i => $err) {
                    if (substr($err, 0, 9) == 'v:1 t:CON') {
                        unset($stderr[$i]);
                    }
                }
                if (count($stderr)) {
                    $this->stderr(implode(PHP_EOL, $stderr) . PHP_EOL);
                }

                // Return resultcode example
                return $baseAnswers[$path]['result'];
            }
        }

        $this->stderr('4.04 Not Found');
        return 0;
    }
}
