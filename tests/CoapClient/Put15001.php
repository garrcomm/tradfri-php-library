<?php

namespace Tests\CoapClient;

class Put15001 extends AbstractTestResult
{
    /**
     * Runs the test result, returns an exit code (0 = success, 1 = failure)
     *
     * @return integer
     */
    public function run(): int
    {
        // Fetch properties
        $deviceId = $this->path[1];
        $data = json_decode(rawurldecode($this->parameters['e']), true);

        // Does the device exist?
        $baseAnswers = json_decode(file_get_contents(__DIR__ . '/BaseAnswers.json'), true);
        if (!isset($baseAnswers['15001/' . $deviceId])) {
            $this->stderr('4.04 Not Found');
            return 0;
        }

        // For now, we just accept all values
        return 0;
    }
}
