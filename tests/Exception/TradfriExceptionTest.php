<?php

namespace Tests\Exception;

use Garrcomm\Tradfri\Exception\TradfriException;
use PHPUnit\Framework\TestCase;

class TradfriExceptionTest extends TestCase
{
    /**
     * Most tests are already done in other tests that catches exceptions, but the getDebugInfo() remained untested.
     *
     * @return void
     */
    public function testGetDebugInfo(): void
    {
        $debugInfo = [
            'command' => '/my/command',
            'stdout'  => 'Output',
            'stderr'  => 'Error data',
        ];
        $exception = new TradfriException('Message', TradfriException::COAP_CLIENT_ERROR + 1, $debugInfo);
        $this->assertEquals($debugInfo, $exception->getDebugInfo());
    }
}
