<?php

namespace Tests\Service;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriAuthenticationTest extends TestCase
{
    /**
     * Tests the Tradfri Authenticate method
     *
     * @return void
     */
    public function testAuthenticateSuccess(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $result = $tradfri->authenticate('valid-security-code');
        $this->assertArrayHasKey('clientIdentity', $result);
        $this->assertArrayHasKey('privateSharedKey', $result);
    }

    /**
     * Tests the Tradfri Authenticate method
     *
     * @return void
     */
    public function testAuthenticateFail(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::AUTHENTICATION_FAILED);
        $tradfri->authenticate('invalid-security-code');
    }

    // Testing the setClientIdentity method will be covered in all other tests that use this class.
}
