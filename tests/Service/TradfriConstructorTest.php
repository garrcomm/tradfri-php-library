<?php

namespace Tests\Service;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriConstructorTest extends TestCase
{
    /**
     * Tests the Tradfri class constructor with an invalid IP address
     *
     * @return void
     */
    public function testConstructInvalidIp(): void
    {
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::INVALID_IPV4_ADDRESS);
        new Tradfri('abc', realpath(__DIR__ . '/../CoapClient/coap-client'));
    }

    /**
     * Tests the Tradfri class constructor with an invalid CoapClient path
     *
     * @return void
     */
    public function testConstructInvalidClientPath(): void
    {
        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::INVALID_COAP_CLIENT);
        new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient') . '/not-a-coap-client');
    }

    /**
     * Tests the Tradfri class constructor with an unreachable IP address
     *
     * @return void
     */
    public function testConstructUnreachableClientPath(): void
    {
        $tradfri = new Tradfri('127.0.0.2', realpath(__DIR__ . '/../CoapClient/coap-client'));
        // We only connect when something needs to be done, so the exception will happen here
        $this->expectException(TradfriException::class);
        $tradfri->authenticate('valid-security-code');
    }

    // Testing a valid constructor will be covered in all other tests that use this class.
}
