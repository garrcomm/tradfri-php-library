<?php

namespace Tests\Service;

use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriGroup;
use Garrcomm\Tradfri\Model\TradfriScene;
use Garrcomm\Tradfri\Service\Tradfri;
use PHPUnit\Framework\TestCase;

class TradfriGetterTest extends TestCase
{
    /**
     * Tests the listScenes() method
     *
     * @return void
     */
    public function testListScenes(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch scenes
        $scenes = $tradfri->listScenes();

        // Did we get an array with TradfriScene objects?
        $this->assertIsArray($scenes);
        foreach ($scenes as $scene) {
            $this->assertInstanceOf(TradfriScene::class, $scene);
        }
    }

    /**
     * Tests the listDevices() method
     *
     * @return void
     */
    public function testListDevices(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch devices
        $devices = $tradfri->listDevices();

        // Did we get an array with BaseTradfriDevice objects?
        $this->assertIsArray($devices);
        foreach ($devices as $device) {
            $this->assertInstanceOf(BaseTradfriDevice::class, $device);
        }
    }

    /**
     * Tests the listGroups() method
     *
     * @return void
     */
    public function testListGroups(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        // Fetch groups
        $groups = $tradfri->listGroups();

        // Did we get an array with TradfriGroup objects?
        $this->assertIsArray($groups);
        foreach ($groups as $group) {
            $this->assertInstanceOf(TradfriGroup::class, $group);
        }
    }

    /**
     * Tests getScene with an invalid ID
     *
     * @return void
     */
    public function testGetInvalidScene(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::ITEM_NOT_FOUND);
        $tradfri->getScene(404);
    }

    /**
     * Tests getDevice with an invalid ID
     *
     * @return void
     */
    public function testGetInvalidDevice(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::ITEM_NOT_FOUND);
        $tradfri->getDevice(404);
    }

    /**
     * Tests getGroup with an invalid ID
     *
     * @return void
     */
    public function testGetInvalidGroup(): void
    {
        $tradfri = new Tradfri('127.0.0.1', realpath(__DIR__ . '/../CoapClient/coap-client'));
        $tradfri->setClientIdentity('50485054726164667269436c69656e74', 'secretPrivateSharedKey');

        $this->expectException(TradfriException::class);
        $this->expectExceptionCode(TradfriException::ITEM_NOT_FOUND);
        $tradfri->getGroup(404);
    }

    // getScene and writeToGroup are tested in TradfriSceneTest
    // getDevice and writeToDevice are tested in TradfriLightTest
    // getGroup is tested in TradfriGroupTest
}
