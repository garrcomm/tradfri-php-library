@echo off
rem This is a Docker shortcut for running Composer on this project

docker.exe run --rm --interactive --tty --volume "%~dp0\..:/var/www/html" garrcomm/php-apache-composer composer %*
if "%ERRORLEVEL%" == "9009" (
    echo Docker for Windows is not installed.
    goto :eof
)
