@echo off
rem This is a Docker shortcut for running PHP Documentor on this project

docker.exe run --rm --interactive --tty --volume "%~dp0\..:/data" phpdoc/phpdoc:3 %*
if "%ERRORLEVEL%" == "9009" (
    echo Docker for Windows is not installed.
    goto :eof
)
